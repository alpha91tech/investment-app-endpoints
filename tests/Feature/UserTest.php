<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Passport;

use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A test that logged in user can access profile.
     *
     * @return void
     */
    public function test_can_get_profile()
    {
        Passport::actingAs(
            User::find(2),
            ['v1/auth-user']
        );
 
        $response = $this->json("GET", "/api/v1/auth-user");

        if ($response->getStatusCode() == 401) {
            $response->assertStatus(401)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {     
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        } 
    }

    /**
     * A test that logged in user can fetch users.
     *
     * @return void
     */
    public function test_can_fetch_users()
    {
        Passport::actingAs(
            User::first(),
            ['v1/users']
        );

        $response = $this->json("GET", "/api/v1/users");
            
        $response->assertStatus(200)->assertJsonStructure(
            [
                'success',
                'code',
                'data',
                'message',
            ]
        );
    }

    /**
     * A test that logged in user can add new user.
     *
     * @return void
     */
    public function test_can_create_user()
    {
        Passport::actingAs(
            User::first(),
            ['v1/user/new']
        );

        $faker = \Faker\Factory::create();
        
        $request = [
            "firstname" => $faker->name(),
            "lastname" => $faker->name(),
            "email" => $faker->safeEmail(),
            "phone" => $faker->numerify("###########"),
            "password" => "@Faker1PWD"
        ];
 
        $response = $this->json("POST", "/api/v1/user/new", $request);
        
        if ($response->getStatusCode() == 400) {
            $response->assertStatus(400)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

    /**
     * A test that logged in user can view other user detail.
     *
     * @return void
     */
    public function test_can_view_user_detail()
    {
        $faker = \Faker\Factory::create();
        $id = $faker->numerify("#");

        Passport::actingAs(
            User::first(),
            ['v1/user/'. $id]
        );
 
        $response = $this->json("GET", "/api/v1/user/{$id}");
        
        if ($response->getStatusCode() == 503) {
            $response->assertStatus(503)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

    /**
     * A test that logged in user can update level 2 user detail.
     *
     * @return void
     */
    public function test_can_update_other_user()
    {
        Passport::actingAs(
            User::first(),
            ['v1/user/7/update']
        );

        $faker = \Faker\Factory::create();
        
        $request = [
            "firstname" => $faker->name(),
            "lastname" => $faker->name(),
            "phone" => $faker->numerify("###########"),
        ];
 
        $response = $this->json("PATCH", "/api/v1/user/7/update", $request);
        
        if ($response->getStatusCode() == 400) {
            $response->assertStatus(400)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

    /**
     * A test that logged in user can disable user.
     *
     * @return void
     */
    public function test_can_disable_user()
    {
        Passport::actingAs(
            User::first(),
            ['v1/user/7/disable']
        );

        $response = $this->json("PATCH", "/api/v1/user/7/disable", $request);
        
        if ($response->getStatusCode() == 503) {
            $response->assertStatus(503)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

    /**
     * A test that logged in user can enable user.
     *
     * @return void
     */
    public function test_can_enable_user()
    {
        Passport::actingAs(
            User::first(),
            ['v1/user/7/enable']
        );

        $response = $this->json("PATCH", "/api/v1/user/7/enable", $request);
        
        if ($response->getStatusCode() == 503) {
            $response->assertStatus(503)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

    /**
     * A test that a user can logout.
     *
     * @return void
     */
    public function test_can_logout()
    {
        Passport::actingAs(
            User::first(),
            ['v1/logout']
        );

        $response = $this->get('api/v1/logout');
        $response->assertStatus(200)->assertJsonStructure(
            [
                'success',
                'code',
                'data',
                'message',
            ]
        );
    }
    
    /**
     * A test for unauthorized acccess for a non admin users.
     *
     * @return void
     *
     * @dataProvider unAuthorizedAccessProviders
     */
    public function test_is_user_not_authorized($input, $method = "GET", $request_body)
    {
        Passport::actingAs(
            User::find(2),
            ['v1/'.$input]
        );

        $response = !empty($request_body) ? 
            $this->json($method, '/api/v1/'.$input, $request_body) : 
            $this->json($method, '/api/v1/'.$input);
 
        $response->assertStatus(401)->assertJsonStructure(
            [
                'error',
                'code',
                'errors',
                'message',
            ]
        );
    }
     
    public function unAuthorizedAccessProviders()
    {
        $faker = \Faker\Factory::create();
        return [
            "cannot get profile" => ['auth-user', 'GET', []],
            "cannot fetch list of users" => ['users', 'GET', []],
            "cannot create a user" => ['user/new', 'POST', [
                    "firstname" => $faker->name(),
                    "lastname" => $faker->name(),
                    "email" => $faker->safeEmail(),
                    "phone" => $faker->numerify("###########"),
                    "password" => "@Faker1PWD"
                ]
            ],
            "cannot update a user" => ['user/7/update', 'PATCH', [
                    "firstname" => $faker->name(),
                    "lastname" => $faker->name(),
                    "phone" => $faker->numerify("###########")
                ]
            ],
            "cannot disable user" => ["user/". $faker->numerify("#")."/disable", "PATCH", []],
            "cannot enable user" => ["user/". $faker->numerify("#")."/enable", "PATCH", []],
            "cannot view user detail" => ["user/". $faker->numerify("#"), "GET", []],
            "cannot add a group" => ['group/new', 'POST', []]
        ];
    }
}
