<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Database\Factories\MenuFactory;
use Laravel\Passport\Passport;

use App\Models\User;
use App\Models\Group;
use App\Models\Menu;

class MenuTest extends TestCase
{
    /**
    * A test that logged in user can fetch menus.
    *
    * @return void
    */
    public function test_can_fetch_menus()
    {
        Passport::actingAs(
            User::first(),
            ['v1/menus']
        );

        $response = $this->json("GET", "/api/v1/menus");
           
        $response->assertStatus(200)->assertJsonStructure(
            [
                'success',
                'code',
                'data',
                'message',
            ]
        );
    }

    /**
    * A test that logged in user can create menu.
    *
    * @return void
    */
    public function test_can_create_menu()
    {
        Passport::actingAs(
            User::first(),
            ['v1/menu/1/new']
        );

        $menu = new MenuFactory;

        $response = $this->json("POST", "/api/v1/menu/1/new", $menu->definition());
           
        $response->assertStatus(200)->assertJsonStructure(
            [
                'success',
                'code',
                'data',
                'message',
            ]
        );
    }
     
    /**
     * A test for unauthorized acccess for a non admin users.
     *
     * @return void
     *
     * @dataProvider unAuthorizedAccessProviders
     */
    public function test_is_user_not_authorized($input, $method = "GET", $request_body)
    {
         Passport::actingAs(
             User::find(2),
             ['v1/'.$input]
         );
 
         $response = !empty($request_body) ? 
             $this->json($method, '/api/v1/'.$input, $request_body) : 
             $this->json($method, '/api/v1/'.$input);
  
         $response->assertStatus(401)->assertJsonStructure(
             [
                 'error',
                 'code',
                 'errors',
                 'message',
             ]
         );
    }

    public function unAuthorizedAccessProviders()
    {
        $menu =  new MenuFactory;
        // $group = Group::find(1);
        return [
            "cannot fetch list of menus" => ['menus', "GET", []],
            "cannot create a menu" => ["menu/1/new", "POST", $menu->definition()],
            "cannot update a menu" => ["menu/2/update", "PATCH", $menu->definition()],
            "cannot delete menu" => ["menu/2/delete", "DELETE", []],
        ];
    }
}
