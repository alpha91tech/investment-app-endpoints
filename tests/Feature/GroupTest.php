<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

// use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\GroupFactory;
use Laravel\Passport\Passport;

use App\Models\User;
use App\Models\Group;

class GroupTest extends TestCase
{
    /**
    * A test that logged in user can fetch groups.
    *
    * @return void
    */
    public function test_can_fetch_groups()
    {
        Passport::actingAs(
            User::first(),
            ['v1/groups']
        );

        $response = $this->json("GET", "/api/v1/groups");
           
        $response->assertStatus(200)->assertJsonStructure(
            [
                'success',
                'code',
                'data',
                'message',
            ]
        );
    }

    /**
    * A test that logged in user can create group.
    *
    * @return void
    */
    public function test_can_create_group()
    {
        Passport::actingAs(
            User::first(),
            ['v1/group/new']
        );

        $group = new GroupFactory;

        $response = $this->json("POST", "/api/v1/group/new", $group->definition());
           
        $response->assertStatus(200)->assertJsonStructure(
            [
                'success',
                'code',
                'data',
                'message',
            ]
        );
    }

    /**
    * A test that logged in user can update a group.
    *
    * @return void
    */
    public function test_can_update_group()
    {
        Passport::actingAs(
            User::first(),
            ['v1/group/2/update']
        );

        $group = new GroupFactory;

        $response = $this->json("PATCH", "/api/v1/group/2/update", $group->definition());
        
        if ($response->getStatusCode() == 503) {
            $response->assertStatus(503)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

    /**
    * A test that logged in user can view a group.
    *
    * @return void
    */
    public function test_can_view_group()
    {
        Passport::actingAs(
            User::first(),
            ['v1/group/2']
        );

        $response = $this->json("GET", "/api/v1/group/2");
        
        if ($response->getStatusCode() == 503) {
            $response->assertStatus(503)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

    /**
     * A test that logged in user can access a group resource.
     *
     * @return void
     */
    public function test_can_access_group()
    {
        Passport::actingAs(
            User::first(),
            ['v1/group/2/access']
        );

        $response = $this->json("GET", "/api/v1/group/2/access");
           
        if ($response->getStatusCode() == 503) {
            $response->assertStatus(503)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

    /**
     * A test that logged in user can delete a group.
     *
     * @return void
     */
    public function test_can_delete_group()
    {
        Passport::actingAs(
            User::first(),
            ['v1/group/2/delete']
        );

        $response = $this->json("DELETE", "/api/v1/group/2/delete");
        
        if ($response->getStatusCode() == 503) {
            $response->assertStatus(503)->assertJsonStructure(
                [
                    'error',
                    'code',
                    'errors',
                    'message',
                ]
            );
        } else {
            $response->assertStatus(200)->assertJsonStructure(
                [
                    'success',
                    'code',
                    'data',
                    'message',
                ]
            );
        }
    }

     
    /**
     * A test for unauthorized acccess for a non admin users.
     *
     * @return void
     *
     * @dataProvider unAuthorizedAccessProviders
     */
    public function test_is_user_not_authorized($input, $method = "GET", $request_body)
    {
         Passport::actingAs(
             User::find(2),
             ['v1/'.$input]
         );
 
         $response = !empty($request_body) ? 
             $this->json($method, '/api/v1/'.$input, $request_body) : 
             $this->json($method, '/api/v1/'.$input);
  
         $response->assertStatus(401)->assertJsonStructure(
             [
                 'error',
                 'code',
                 'errors',
                 'message',
             ]
         );
    }

    public function unAuthorizedAccessProviders()
    {
        $group =  new GroupFactory;
        return [
            "cannot fetch list of groups" => ['groups', "GET", []],
            "cannot create a group" => ['group/new', "POST", $group->definition()],
            "cannot update a group" => ['group/2/update', "PATCH", $group->definition()],
            "cannot delete group" => ["group/2/delete", "DELETE", []],
            "cannot view group detail" => ["group/2", "GET", []],
            "cannot access group resource" => ["group/2/access", "GET", []],
        ];
    }
}
