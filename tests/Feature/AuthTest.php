<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\User;

class AuthTest extends TestCase
{
    /**
     * A test api endpoint is active.
     *
     * @return void
     */
    public function test_can_access_api()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json("GET", "/api/v1/home");

        $response->assertStatus(200)->assertJson(
            [
                'success' => true,
                'code'    => 200,
                'data' => ["status" => "Ok"],
                'message' => "API response success",
            ]
        );
    }
    
    /**
     * A test login enpoint on success.
     *
     * @return void
     */
    public function test_can_login()
    {
        $request = [
            "email" => "admin@investmentone.com",
            "password" => "1nv3stm3nt1"
        ];

        $response = $this->json("POST", "/api/v1/login", $request, ['Accept' => 'application/json']);
 
        $response->assertStatus(200)->assertJsonStructure(
            [
                'success',
                'code',
                'data' => ["user", "token"],
                'message',
            ]
        );
        
        $this->assertAuthenticated();
    }
    
    /**
     * A test login enpoint on failed.
     *
     * @return void
     */
    public function test_cannot_login_authentication_error()
    {
        $faker = \Faker\Factory::create();;

        $request = [
            "email" => $faker->safeEmail(),
            "password" => $faker->password()
        ];
 
 
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json("POST", "/api/v1/login", $request);
  
        $response->assertStatus(401)->assertJsonStructure(
            ['error', 'code','error', 'message']
        );
    }
    
    /**
     * A test that a user can register.
     *
     * @return void
     */
    public function test_can_register()
    {
        $faker = \Faker\Factory::create();
        
        $request = [
            "firstname" => $faker->name(),
            "lastname" => $faker->name(),
            "email" => $faker->safeEmail(),
            "phone" => $faker->numerify("###########"),
            "password" => "@Faker1PWD"
        ];

        $response = $this->json("POST", "/api/v1/register", $request, [ 'Accept' => 'application/json']);
 
        $response->assertStatus(200)->assertJsonStructure([
            'success',
            'code',
            'data',
            'message',
        ]);
    }
    
    /**
     * A test that a user cannot register invalid credevtials.
     *
     * @return void
     */
    public function test_cannot_register_validation_error() // passed
    {
        $request = [
            "firstname" => "John",
            "lastname" => "Doe",
            "password" => "J0hnd032233"
        ];

        $response = $this->json("POST", "/api/v1/register", $request, ['Accept' => 'application/json']);
 
        $response->assertStatus(422)->assertJsonStructure([
            'errors',
            'message',
        ]);
    }

    
    /**
     * A test for unauthorized acccess and record not found.
     *
     * @return void
     *
     * @dataProvider unAuthorizedAccessProviders
     */
    public function test_is_user_not_authorized($input, $method = "GET", $request_body)
    {
        $response = !empty($request_body) ? 
            $this->json($method, 'api/v1/'.$input, $request_body) : 
            $this->json($method, 'api/v1/'.$input);

        $response->assertStatus(401)->assertJsonStructure(
            []
        );
    }
    
    public function unAuthorizedAccessProviders()
    {
        $faker = \Faker\Factory::create();
        return [
            ['logout', 'GET', []],
            ['users', 'GET', []],
            ['groups', 'GET', []],
            ['modules', 'GET', []],
            ['sections', 'GET', []],
            ['menus', 'GET', []],
        ];
    }
}
