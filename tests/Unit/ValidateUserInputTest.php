<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\Http\Requests\UserEntryRequest;
use Illuminate\Database\Eloquent\Factories\Factory;

use Illuminate\Support\Facades\Validator;

class ValidateUserInputTest extends TestCase
{
    /**
     * A valiated user request.
     *
     * @return void
     *
     * @test
     */
    public function is_a_valid_user_input()
    {
        $request = new UserEntryRequest();

        $faker = \Faker\Factory::create();
        $input = [
            'firstname' => $faker->name(),
            'lastname' => $faker->name(),
            'email' => $faker->safeEmail(), 
            'phone' => $faker->numerify("###########"), 
            'password' => '@Faker1PWD'
        ];

        $validator = Validator::make($input, $request->rules());

        $this->assertTrue($validator->passes());
    }

    /**
     * A invalid user request.
     *
     * @return void
     *
     * @dataProvider requestDataEntry
     *
     * @test
     */
    public function is_not_a_valid_user_input($input, $check)
    {
        $request = new UserEntryRequest();

        $validator = Validator::make($input, $request->rules());

        $this->assertFalse($validator->passes());
        $this->assertContains($check, $validator->errors()->keys());
    }

    public function requestDataEntry()
    {
        $faker = \Faker\Factory::create();
        return [
            "test firstname is empty" => [
                [
                    'firstname' => '',
                    'lastname' => $faker->name(),
                    'email' => $faker->safeEmail(), 
                    'phone' => $faker->numerify("###########"), 
                    'password' => '@Faker1PWD'
                ],
                'firstname'
            ],
            "test lastname is empty" => [
                [
                    'firstname' => $faker->name(),
                    'lastname' => '',
                    'email' => $faker->safeEmail(), 
                    'phone' => $faker->numerify("###########"), 
                    'password' => '@Faker1PWD'
                ],
                'lastname'
            ],
            "test email is empty" => [
                [
                    'firstname' => $faker->name(),
                    'lastname' => $faker->name(),
                    'email' => '', 
                    'phone' => $faker->numerify("###########"), 
                    'password' => '@Faker1PWD'
                ],
                'email'
            ],
            "test email is not a valid email" => [
                [
                    'firstname' => $faker->name(),
                    'lastname' => $faker->name(),
                    'email' => $faker->name(), 
                    'phone' => $faker->numerify("###########"), 
                    'password' => '@Faker1PWD'
                ],
                'email'
            ],
            "test phone number is empty" => [
                [
                    'firstname' => $faker->name(),
                    'lastname' => $faker->name(),
                    'email' => $faker->safeEmail(), 
                    'phone' => '', 
                    'password' => '@Faker1PWD'
                ],
                'phone'
            ],
            "test phone number is not valid or not supported" => [
                [
                    'firstname' => $faker->name(),
                    'lastname' => $faker->name(),
                    'email' => $faker->name(), 
                    'phone' => $faker->numerify("A#########"), 
                    'password' => '@Faker1PWD'
                ],
                'phone'
            ],
            "test password is empty" => [
                [
                    'firstname' => $faker->name(),
                    'lastname' => $faker->name(),
                    'email' => $faker->safeEmail(), 
                    'phone' => $faker->numerify("###########"), 
                    'password' => ''
                ],
                'password'
            ],
            "test password is not valid or not supported" => [
                [
                    'firstname' => $faker->name(),
                    'lastname' => $faker->name(),
                    'email' => $faker->name(), 
                    'phone' => $faker->numerify("##########!"), 
                    'password' => 'password'
                ],
                'password'
            ],
        ];
    }
}
