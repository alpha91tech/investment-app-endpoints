<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\Http\Requests\SectionEntryRequest;
use Illuminate\Database\Eloquent\Factories\Factory;

use Illuminate\Support\Facades\Validator;

class ValidateSectionInputTest extends TestCase
{
    /**
     * A valiated section request.
     *
     * @return void
     *
     * @test
     */
    public function is_a_valid_section_input()
    {
        $request = new SectionEntryRequest;

        $faker = \Faker\Factory::create();
        $input = [
            'title' => $faker->name(),
            'description' => $faker->name()
        ];

        $validator = Validator::make($input, $request->rules());

        $this->assertTrue($validator->passes());
    }

    /**
     * A invalid section request.
     *
     * @return void
     *
     * @dataProvider requestDataEntry
     *
     * @test
     */
    public function is_not_a_valid_section_input($input, $check)
    {
        $request = new SectionEntryRequest;

        $validator = Validator::make($input, $request->rules());
        
        if ($validator->passes()) {
            $this->assertTrue($validator->passes());
        } else {
            $this->assertFalse($validator->passes());
            $this->assertContains($check, $validator->errors()->keys());
        }
    }

    public function requestDataEntry()
    {
        $faker = \Faker\Factory::create();
        return [
            "test title is empty" => [
                [
                    'title' => '',
                    'description' => $faker->name(),
                ],
                'title'
            ]
        ];
    }
}
