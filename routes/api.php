<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['throttle:100,1', 'json.guard', 'cors']], function () {

    Route::group(['middleware' => ['guest:api']], function () {
        /* Guest */
        Route::get(config('auth.mode').'/{index}', 'App\Http\Controllers\HomeController@index')->where("index", "index|home|")->name('index');

        /* Authenticarion Routes*/
        Route::post(config('auth.mode').'/{register}', 'App\Http\Controllers\Auth\RegisterController@register')->where("register", "register|signup")->name('auth.register');
        Route::post(config('auth.mode').'/{login}', 'App\Http\Controllers\Auth\LoginController@authenticate')->where("login", "login|signin")->name('auth.login');

        /* Password Reset*/
        Route::post(config('auth.mode').'/forgot-password', 'App\Http\Controllers\Auth\ForgotPasswordController@reset_link')->name('password.email');
        Route::patch(config('auth.mode').'/reset-password', 'App\Http\Controllers\Auth\ResetPasswordController@update')->name('password.update');

    });

    /* Post Authentication*/
    Route::group(['middleware' => ['auth:api']], function () {
        /* User Routes*/
        Route::group(['middleware' => ['admin.guard']], function () {
            Route::get(config('auth.mode').'/users', 'App\Http\Controllers\API\UserController@index')->name('users');
            Route::post(config('auth.mode').'/user/new', 'App\Http\Controllers\API\UserController@store')->name('user.create');
            Route::get(config('auth.mode').'/user/{id}', 'App\Http\Controllers\API\UserController@show')->name('user.show');
            Route::patch(config('auth.mode').'/user/{id}/update', 'App\Http\Controllers\API\UserController@update')->name('user.update');
            Route::patch(config('auth.mode').'/user/{id}/disable', 'App\Http\Controllers\API\UserController@disable')->name('user.disable');
            Route::patch(config('auth.mode').'/user/{id}/enable', 'App\Http\Controllers\API\UserController@enable')->name('user.enable');
            Route::put(config('auth.mode').'/group/{id}/add/{user}', 'App\Http\Controllers\API\UserController@add_to_group')->name('user.group.add');
            Route::delete(config('auth.mode').'/group/{id}/remove/{user}', 'App\Http\Controllers\API\UserController@remove_from_group')->name('user.group.remove');
        });
        Route::patch(config('auth.mode').'/user/update', 'App\Http\Controllers\API\UserController@self_update')->name('user.self.update');
        Route::get(config('auth.mode').'/auth-user', 'App\Http\Controllers\API\UserController@auth_user')->name('auth-user'); // get logged in user detail
        Route::get(config('auth.mode').'/logout', 'App\Http\Controllers\API\UserController@logout')->name('logout');

        /* Group Routes*/
        Route::get(config('auth.mode').'/groups', 'App\Http\Controllers\API\GroupController@index')->name('groups');
        Route::post(config('auth.mode').'/group/new', 'App\Http\Controllers\API\GroupController@store')->middleware('admin.guard')->name('group.create');
        Route::get(config('auth.mode').'/group/{id}', 'App\Http\Controllers\API\GroupController@show')->name('group.show');
        Route::patch(config('auth.mode').'/group/{id}/update', 'App\Http\Controllers\API\GroupController@update')->middleware('admin.guard')->name('group.update');
        Route::delete(config('auth.mode').'/group/{id}/delete', 'App\Http\Controllers\API\GroupController@destroy')->middleware('admin.guard')->name('group.delete');
        Route::get(config('auth.mode').'/group/{id}/access', 'App\Http\Controllers\API\GroupController@access')->name('group.access');

        /* Module Routes*/
        Route::get(config('auth.mode').'/modules', 'App\Http\Controllers\API\ModuleController@index')->name('modules');
        Route::post(config('auth.mode').'/module/{group}/new', 'App\Http\Controllers\API\ModuleController@store')->name('module.create');
        Route::patch(config('auth.mode').'/module/{id}/update', 'App\Http\Controllers\API\ModuleController@update')->name('module.update');
        Route::delete(config('auth.mode').'/module/{id}/delete', 'App\Http\Controllers\API\ModuleController@destroy')->name('module.delete');

        /* Section Routes*/
        Route::get(config('auth.mode').'/sections', 'App\Http\Controllers\API\SectionController@index')->name('sections');
        Route::post(config('auth.mode').'/section/{group}/new', 'App\Http\Controllers\API\SectionController@store')->name('section.create');
        Route::patch(config('auth.mode').'/section/{id}/update', 'App\Http\Controllers\API\SectionController@update')->name('section.update');
        Route::delete(config('auth.mode').'/section/{id}/delete', 'App\Http\Controllers\API\SectionController@destroy')->name('section.delete');

        /* Menu Routes*/
        Route::get(config('auth.mode').'/menus', 'App\Http\Controllers\API\MenuController@index')->name('menus');
        Route::post(config('auth.mode').'/menu/{group}/new', 'App\Http\Controllers\API\MenuController@store')->name('menu.create');
        Route::patch(config('auth.mode').'/menu/{id}/update', 'App\Http\Controllers\API\MenuController@update')->name('menu.update');
        Route::delete(config('auth.mode').'/menu/{id}/delete', 'App\Http\Controllers\API\MenuController@destroy')->name('menu.delete');
    });

});
