<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class AdminGuard
{
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user() && auth()->user()->role == true) {
            $response = [
                'error' => true,
                'code'    => 401,
                'message' => 'Unauthorized Access',
                'errors' => ['error' => 'User not authorized to access this route'],
            ];
            return response()->json($response, 401);
        }

        return $next($request);
    }
}
