<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function responseSuccess(
        $result = ["status" => "Ok"], 
        $message = "API response success", 
        $code = 200, 
        $content_type  = 'application/json'
    )
    {
    	$response = [
            'success' => true,
            'code'    => $code,
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response, 200)->withHeaders([
            'Content-Type' => $content_type,
            'Accept' => $content_type,
            'Content-Language' => 'en',
            'App-Token' => request()->header("app-token"),
            'Authorization' => request()->header("Authorization"),
            'Date' => date('Y-m-d H:i:s'),
            'Status' => '200'
        ]);
    }

    public function responseError(
        $error = "Internal Server Error", 
        $error_data = ["error" => "server error"], 
        $code = '500', 
        $content_type  = 'application/json'
    )
    {
    	$response = [
            'error' => true,
            'code'    => $code,
            'errors' => $error_data,
            'message' => $error,
        ];

        return response()->json($response, $code)->withHeaders([
            'Content-Type' => $content_type,
            'Content-Language' => 'en',
            'App-Token' => request()->header("app-token"),
            'Date' => date('Y-m-d H:i:s'),
            'Status' => $code
        ]);
    }
    
}
