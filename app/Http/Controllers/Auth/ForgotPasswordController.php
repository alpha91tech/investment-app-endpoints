<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Validator;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function reset_link(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'email' => ['required','email'],
        ]);

        if($validator->fails()){
            return $this->responseError('Bad Request: Validation Error.', $validator->errors(), 400);       
        }
        try {
            $status = Password::sendResetLink(
                $request->only('email')
            );

            if ($status !== Password::RESET_LINK_SENT) {
                return $this->responseError('Bad Request Email: Invalid Email.', ["error" => 'Unable to send password reset link to email'], 400);  
            }

            $status = $status === Password::RESET_LINK_SENT ? ['status' => __($status)] : ['email' => __($status)];

            return $this->responseSuccess($status, 'Password reset link sent to user email.');
        } catch (\Throwable $th) {
            return $this->responseError('Email Validation Error: Validation Error.', ["error" => "Error has occured from mail server"], 400); 
        }
    }
}
