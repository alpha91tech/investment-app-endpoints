<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) { 
            $user = Auth::user(); 
            $token = Auth::user()->createToken('InvestOneApp')->accessToken;
            
            $response = ["user" => $user, "token" => $token];
            
            return $this->responseSuccess($response, 'User successfully logged in.');
        } else { 
            return $this->responseError('Unauthorised Access.', ['error'=>'Invalid login detail'], 401);
        } 
    }
    
}
