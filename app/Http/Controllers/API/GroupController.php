<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Group;

use App\Http\Requests\GroupEntryRequest;

class GroupController extends Controller
{
    public function index()
    {
        $users = Group::paginate(10);
        return $this->responseSuccess($users,"List of groups fetched successfully");
    }
    
    public function store(GroupEntryRequest $request)
    {
        $data = $request->validated();
                
        $group = $this->create($data);

        return $this->responseSuccess($group, "Group created successfully");
    }
    
    protected function create(array $data)
    {
        return Group::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'created_by' => auth()->user()->id, 
        ]);
    }
    
    public function show($id)
    {
        $group = Group::find($id);

        if (empty($group)) {
            return $this->notFound($group);
        }

        return $this->responseSuccess($group, "Group detail fetched successfully");
    }
    
    public function update(GroupEntryRequest $request, $id)
    {
        $group = Group::find($id);

        if (empty($group)) {
            return $this->notFound($group);
        }

        $data = $request->validated();

        $response = $this->modify($data, $group);

        return $this->responseSuccess($response, "Group detail modified successfully");
    }
    
    protected function modify(array $data, $group)
    {
        $group->update([
            'title' => $data['title'],
            'description' => $data['description'],
        ]);

        return $group;
    }
    
    public function destroy($id)
    {
        $group = Group::find($id);

        if (empty($group)) {
            return $this->notFound($group);
        }

        Group::destroy($id);

        return $this->responseSuccess($group, "Group detail deleted successfully");
    }

    public function access($id)
    {
        $group = Group::find($id);

        if (empty($group)) {
            return $this->notFound($group);
        }

        $response = [
            "modules" => $group->modules(),
            "sections" => $group->sections(),
            "menus" => $group->menus(),
        ];

        return $this->responseSuccess($response, "Group access fetched successfully");
    }

    protected function notFound($group)
    {
        return $this->responseError(
            "Internal Server Error", 
            ["group" => "Group detail not found"], 
            '503'
        );
    }
}
