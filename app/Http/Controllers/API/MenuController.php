<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Menu;
use App\Models\Group;

use App\Http\Requests\MenuEntryRequest;

class MenuController extends Controller
{
    public function index()
    {
        $menus = Menu::paginate(10);
        return $this->responseSuccess($menus,"List of menus fetched successfully");
    }
    
    public function store(MenuEntryRequest $request, $group)
    {
        $group = Group::find($group);

        if (empty($group)) {
            return $this->notFound(["group" => "Group detail not found"]);
        }

        $data = $request->validated();
                
        $menu = $this->create($data, $group->id);

        return $this->responseSuccess($menu, "Menu created successfully");
    }
    
    protected function create(array $data, $id)
    {
        return Menu::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'created_by' => auth()->user()->id, 
            'group_id' => $id,
        ]);
    }
    
    public function show($id)
    {
        $menu = Menu::find($id);

        if (empty($menu)) {
            return $this->notFound();
        }

        $menu->group = $menu->group();

        return $this->responseSuccess($menu, "Menu detail modified successfully");
    }
    
    public function update(MenuEntryRequest $request, $id)
    {
        $menu = Menu::find($id);

        if (empty($menu)) {
            return $this->notFound();
        }

        $data = $request->validated();

        $response = $this->modify($data, $menu);

        return $this->responseSuccess($response, "Menu detail modified successfully");
    }
    
    protected function modify(array $data, $menu)
    {
        $menu->update([
            'title' => $data['title'],
            'description' => $data['description'],
        ]);

        return $menu;
    }
    
    public function destroy($id)
    {
        $menu = Menu::find($id);

        if (empty($menu)) {
            return $this->notFound();
        }

        Menu::destroy($id);

        return $this->responseSuccess($menu, "Menu detail deleted successfully");
    }

    protected function notFound(
        $data = ["menu" => "Menu detail not found"]
    )
    {
        return $this->responseError(
            "Internal Server Error", 
            $data, 
            '503'
        );
    }
}
