<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Module;
use App\Models\Group;

use App\Http\Requests\ModuleEntryRequest;

class ModuleController extends Controller
{
    public function index()
    {
        $users = Module::paginate(10);
        return $this->responseSuccess($users,"List of modules fetched successfully");
    }
    
    public function store(ModuleEntryRequest $request, $group)
    {
        $group = Group::find($group);

        if (empty($group)) {
            return $this->notFound(["group" => "Group detail not found"]);
        }

        $data = $request->validated();
                
        $module = $this->create($data, $group->id);

        return $this->responseSuccess($module, "Module created successfully");
    }
    
    protected function create(array $data, $id)
    {
        return Module::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'created_by' => auth()->user()->id, 
            'group_id' => $id,
        ]);
    }
    
    public function show($id)
    {
        $module = Module::find($id);

        if (empty($module)) {
            return $this->notFound();
        }

        $module->group = $module->group();

        return $this->responseSuccess($module, "Module detail modified successfully");
    }
    
    public function update(ModuleEntryRequest $request, $id)
    {
        $module = Module::find($id);

        if (empty($module)) {
            return $this->notFound();
        }

        $data = $request->validated();

        $response = $this->modify($data, $module);

        return $this->responseSuccess($response, "Module detail modified successfully");
    }
    
    protected function modify(array $data, $module)
    {
        $module->update([
            'title' => $data['title'],
            'description' => $data['description'],
        ]);

        return $module;
    }
    
    public function destroy($id)
    {
        $module = Module::find($id);

        if (empty($module)) {
            return $this->notFound();
        }

        Module::destroy($id);

        return $this->responseSuccess($module, "Module detail deleted successfully");
    }

    protected function notFound(
        $data = ["module" => "Module detail not found"]
    )
    {
        return $this->responseError(
            "Internal Server Error", 
            $data, 
            '503'
        );
    }
}
