<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\UserEntryRequest;

use App\Models\User;
use App\Models\Group;
use App\Models\Privilege;

class UserController extends Controller
{

    public function __construct()
    {}
    
    public function index()
    {
        $users = User::where([["role", true]])->paginate(10);

        return $this->responseSuccess($users,"List of users fetched successfully");
    }
    
    public function auth_user()
    {
        if (empty(auth()->user())) {
            return $this->responseError(
                "Authorization Error", ['error'=>'User not logged in'], 401
            );
        }
        return $this->responseSuccess(auth()->user(),"Authenticated user detail fetched");
    }
    
    public function store(UserEntryRequest $request)
    {
        $data = $request->validated();
                
        $user = $this->create(request()->all()); 

        return $this->responseSuccess($user, "User created successfully");
    }

    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(60),
        ]);
    }
    
    public function show($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            return $this->notFound();
        }

        return $this->responseSuccess($user, "User detail fetched successfully");
    }
    
    public function update(UserEntryRequest $request, $id)
    {
        $user = User::find($id);
        
        if (empty($user)) {
            return $this->notFound($user);
        }

        $user->update([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'phone' => $request->phone
        ]);

        return $this->responseSuccess($user, "User detail updated successfully");
    }

    public function self_update(Request $request)
    {
        $user = User::find(auth()->user()->id);
        
        if (empty($user)) {
            return $this->notFound($user);
        }

        $user->update([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'phone' => $request->phone
        ]);

        return $this->responseSuccess($user, "Profile updated successfully");
    }
    
    public function disable($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            return $this->notFound($user);
        }
        $is_disabled = $user->disabled;

        if ($user->disabled == false) {
            $user->update([
                "disabled" => true
            ]);
        }
        
        if ($is_disabled == true) {
            return $this->responseError(
                "Internal Server Error", ['error'=>'User was already disabled'], 503
            );
        } else {
            return $this->responseSuccess($user, "User successfully disabled");
        }
    }
    
    public function enable($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            return $this->notFound();
        }
        $is_disabled = $user->disabled;

        if ($user->disabled == true) {
            $user->update([
                "disabled" => false
            ]);
        }
        
        if ($is_disabled == true) {
            return $this->responseError(
                "Internal Server Error", ['error'=>'User was already enabled'], 503
            );
        } else {
            return $this->responseSuccess($user, "User successfully enabled");
        }
    }

    public function add_to_group($id, $user)
    {
        $user = User::find($user);
        
        if (empty($user)) {
            return $this->notFound();
        }

        $group = Group::find($id);

        if (empty($group)) {
            return $this->notFound(["group" => "Group detail not found"]);
        }

        $privilegeExists = Privilege::where([
            ["user_id", "=", $user->id],
            ["group_id", "=", $group->id]
        ])->first();

        if (!empty($privilegeExists)) {
            return $this->responseError(
                "Bad Request", 
                ["privilege" => "User already exist in group"], 
                '400'
            );
        }

        $privilege = Privilege::create([
            "user_id" => $user->id,
            "group_id" => $group->id
        ]);

        $privilege->user = $user;
        $privilege->group = $group;

        return $this->responseSuccess($privilege, "User successfully added to group");
    }

    public function remove_from_group($id, $user)
    {
        $user = User::find($user);
        
        if (empty($user)) {
            return $this->notFound();
        }

        $group = Group::find($id);

        if (empty($group)) {
            return $this->notFound(["group" => "Group detail not found"]);
        }

        $privilege = Privilege::where([
            ["user_id", "=", $user->id],
            ["group_id", "=", $group->id]
        ])->first();

        if (empty($privilege)) {
            return $this->responseError(
                "Bad Request", 
                ["privilege" => "User does not exist in group"], 
                '400'
            );
        }

        Privilege::destroy($privilege->id);

        $privilege->user = $user;
        $privilege->group = $group;

        return $this->responseSuccess($privilege, "User successfully removed from group");
    }

    public function logout(Request $request)
    {   
        if (empty(auth()->user())) {
            return $this->responseError(
                "Authorization Error", ['error'=>'User not logged in'], 401
            );
        }
        $user = auth()->user();
        $token = $request->user()->token();
        $token->revoke();

        return $this->responseSuccess($user, "User successfully logged out!");
    }

    protected function notFound($data = ["user" => "User detail not found"])
    {
        return $this->responseError(
            "Internal Server Error", 
            $data, 
            '503'
        );
    }
}
