<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Section;
use App\Models\Group;

use App\Http\Requests\SectionEntryRequest;

class SectionController extends Controller
{
    public function index()
    {
        $users = Section::paginate(10);
        return $this->responseSuccess($users,"List of sections fetched successfully");
    }
    
    public function store(SectionEntryRequest $request, $group)
    {
        $group = Group::find($group);

        if (empty($group)) {
            return $this->notFound(["group" => "Group detail not found"]);
        }

        $data = $request->validated();
                
        $section = $this->create($data, $group->id);

        return $this->responseSuccess($section, "Section created successfully");
    }
    
    protected function create(array $data, $id)
    {
        return Section::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'created_by' => auth()->user()->id, 
            'group_id' => $id,
        ]);
    }
    
    public function show($id)
    {
        $section = Section::find($id);

        if (empty($section)) {
            return $this->notFound();
        }

        $section->group = $section->group();

        return $this->responseSuccess($section, "Section detail modified successfully");
    }
    
    public function update(SectionEntryRequest $request, $id)
    {
        $section = Section::find($id);

        if (empty($section)) {
            return $this->notFound();
        }

        $data = $request->validated();

        $response = $this->modify($data, $section);

        return $this->responseSuccess($response, "Section detail modified successfully");
    }
    
    protected function modify(array $data, $section)
    {
        $section->update([
            'title' => $data['title'],
            'description' => $data['description'],
        ]);

        return $section;
    }
    
    public function destroy($id)
    {
        $section = Section::find($id);

        if (empty($section)) {
            return $this->notFound();
        }

        Section::destroy($id);

        return $this->responseSuccess($section, "Section detail deleted successfully");
    }

    protected function notFound(
        $data = ["section" => "Section detail not found"]
    )
    {
        return $this->responseError(
            "Internal Server Error", 
            $data, 
            '503'
        );
    }
}
