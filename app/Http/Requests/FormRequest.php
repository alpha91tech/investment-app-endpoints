<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest as MainFormRequest;

abstract class FormRequest extends MainFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize();

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\HttpResponseException
     */
    public function failedValidation(Validator $validator) { 
        $response = [
            'error' => false,
            'code'    => "400",
            'errors' => $validator->errors(),
            'message' => "Bad Request: Validation Error",
        ];

        throw new HttpResponseException(response()->json($response, 400)
            ->withHeaders([
                'Content-Type' => 'application/json',
                'Content-Language' => 'en',
                'Authorization' => request()->header("Authorization"),
                'Date' => now(),
                'Status' => 400
            ])
        ); 
    }
}
