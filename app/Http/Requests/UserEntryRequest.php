<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;

class UserEntryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        $authMode = config('auth.mode');
        $pathArray = array($authMode.'/user/new', $authMode.'/register', $authMode.'/signup');
        
        return (in_array(request()->path(), $pathArray) || (config('app.env') == 'testing' && in_array(request()->path(), $pathArray))) ? 
            [
                'firstname' => ['required', 'string', 'max:255'],
                'lastname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['required', 'string', 'min:10', 'max:11', 'unique:users', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
                'password' => ['required', 'string', 'min:8', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/']
            ] :
            [
                'firstname' => ['required', 'string', 'max:255'],
                'lastname' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'unique:users', 'min:10', 'max:11', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            ]
        ;
    }
}
