<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;

class SectionEntryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'title' => 'required|string|min:1'. !empty(request()->id) ? '' : '|unique:groups',
            'description' => ['string']
        ];
    }
}
