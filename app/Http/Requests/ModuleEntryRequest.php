<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;

class ModuleEntryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string|min:1',
            'description' => ['string']
        ];
    }
}
