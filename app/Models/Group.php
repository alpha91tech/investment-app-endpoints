<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    
    protected $guarded = [];

    public function users()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function modules()
    {
        return $this->hasMany('App\Models\Module', 'group_id');
    }

    public function sections()
    {
        return $this->hasMany('App\Models\Section', 'group_id');
    }

    public function menus()
    {
        return $this->hasMany('App\Models\Menu', 'group_id');
    }
    
    public function privileges()
    {
        return $this->hasMany('App\Models\Privilege', 'group_id');
    }
}
