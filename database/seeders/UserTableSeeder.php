<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            User::create([
                'firstname' => 'Admin',
                'lastname' => 'Admin',
                'email' => 'admin@investmentone.com',
                'role' => '0',
                'disabled' => false,
                'email_verified_at' => now(),
                'password' => Hash::make('1nv3stm3nt1'),
            ])
        );
    }
}
